package cajalopez.logisticapp;

import android.content.Context;
import dagger.Module;
import dagger.Provides;

@Module
public class App_ContextModule {

    private Context context;

    public App_ContextModule(Context context)
    {
        this.context = context;
    }

    @Provides
    public Context Context()
    {
     return this.context;
    }

}
