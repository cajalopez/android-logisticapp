package cajalopez.logisticapp.ViewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.zetterstrom.com.forecast.models.Forecast;
import cajalopez.logisticapp.App_Main;
import cajalopez.logisticapp.Repository.Gps.LocationLiveData;
import cajalopez.logisticapp.Repository.UserRepository;
import timber.log.Timber;


public class UserViewModel extends ViewModel
{
    private UserRepository userRepository;

    public void init()
    {

        Timber.e("Iniciando");
        userRepository = App_Main.getInstance().getUserRepository();
        userRepository.getWeather();
    }

    public MutableLiveData<Forecast> getWeathertLiveData() {
        Timber.e("Calling forecast");
        return userRepository.forecast;
    }

    public LocationLiveData getLocation()
    {
     return userRepository.locationLiveData;
    }
}
