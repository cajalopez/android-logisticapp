package cajalopez.logisticapp.Repository.Api;

import android.content.Context;
import android.zetterstrom.com.forecast.ForecastClient;
import android.zetterstrom.com.forecast.ForecastConfiguration;

import java.io.File;

import cajalopez.logisticapp.App_ContextModule;
import cajalopez.logisticapp.App_Scope;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;

@Module(includes = App_ContextModule.class)
public class Forecast_Module
{

    @App_Scope
    @Provides
    public ForecastConfiguration forecastConfiguration(Context context)
    {
        ForecastConfiguration forecastConfiguration = new ForecastConfiguration.Builder("60a0be9005c1b65d51f1bdb587bb033c")
                .setCacheDirectory(context.getCacheDir())
                .build();
        ForecastClient.create(forecastConfiguration);
        return forecastConfiguration;
    }

    @App_Scope
    @Provides
    public File cacheFile(Context context)
    {
        return new File(context.getCacheDir(),"forecast_cache");
    }


    @App_Scope
    @Provides
    public Cache cache(File cacheFile)
    {
        return new Cache(cacheFile,10*1000*1000); // -> 10 MegaBytes Cache
    }



}
