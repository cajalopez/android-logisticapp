package cajalopez.logisticapp.Repository.Gps;

import android.content.Context;

import cajalopez.logisticapp.App_ContextModule;
import cajalopez.logisticapp.App_Scope;
import dagger.Module;
import dagger.Provides;

/**
 * Created by cajalopez on 7/12/17.
 */

@Module(includes = App_ContextModule.class)
public class LocationLiveData_Module
{

    @App_Scope
    @Provides
    public LocationLiveData locationLiveData(Context context)
    {
        return LocationLiveData.get(context);
    }

}
