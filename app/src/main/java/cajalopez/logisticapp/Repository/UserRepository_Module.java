package cajalopez.logisticapp.Repository;

import android.zetterstrom.com.forecast.ForecastConfiguration;
import cajalopez.logisticapp.App_Scope;
import cajalopez.logisticapp.Repository.Api.Forecast_Module;
import cajalopez.logisticapp.Repository.Gps.LocationLiveData;
import cajalopez.logisticapp.Repository.Gps.LocationLiveData_Module;
import dagger.Module;
import dagger.Provides;

@Module(includes = {Forecast_Module.class, LocationLiveData_Module.class})
public class UserRepository_Module {


    @App_Scope
    @Provides
    public UserRepository repository(ForecastConfiguration configuration, LocationLiveData locationLiveData)
    {
        UserRepository userRepository= new UserRepository(configuration,locationLiveData);
        return userRepository;
    }

}
