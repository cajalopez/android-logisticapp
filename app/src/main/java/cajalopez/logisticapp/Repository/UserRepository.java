package cajalopez.logisticapp.Repository;

import android.Manifest;
import android.arch.lifecycle.MutableLiveData;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.zetterstrom.com.forecast.ForecastClient;
import android.zetterstrom.com.forecast.ForecastConfiguration;
import android.zetterstrom.com.forecast.models.Forecast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import javax.inject.Inject;
import javax.inject.Singleton;

import cajalopez.logisticapp.App_Main;
import cajalopez.logisticapp.Repository.Gps.LocationLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

@Singleton
public class UserRepository {
    public MutableLiveData<Forecast> forecast = new MutableLiveData<>();
    public LocationLiveData locationLiveData;
    public ForecastConfiguration configuration;
    public ForecastClient client;
    public FusedLocationProviderClient fusedLocationProviderClient;


    @Inject
    public UserRepository(ForecastConfiguration configuration, LocationLiveData locationLiveData) {
        Timber.e("Iniciando");
        this.configuration = configuration;
        this.locationLiveData = locationLiveData;
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(App_Main.getInstance());
        ForecastClient.create(configuration);
        client = ForecastClient.getInstance();
    }

    public void getWeather() {
        Timber.e("Iniciando getForecast");

        if (ActivityCompat.checkSelfPermission(App_Main.getInstance(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(App_Main.getInstance(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                locationLiveData.setValue(location);
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                client.getForecast(latitude, longitude, new Callback<Forecast>()
                {
                    @Override
                    public void onResponse(Call<Forecast> forecastCall, Response<Forecast> response)
                    {
                        forecast.setValue(response.body());
                        Timber.e(response.body().getTimezone());
                    }

                    @Override
                    public void onFailure(Call<Forecast> forecastCall, Throwable t)
                    {

                    }
                });
            }
        });


    }


}
