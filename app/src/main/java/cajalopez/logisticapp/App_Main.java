package cajalopez.logisticapp;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;

import cajalopez.logisticapp.Repository.UserRepository;
import cajalopez.logisticapp.View.Fragments.Main_Fragment;
import timber.log.Timber;

public class App_Main extends Application {

    private static App_Main instancie;
    private App_Component component;
    private UserRepository userRepository;
    public static synchronized App_Main getInstance()
    {
        if(instancie == null)
        {
            instancie = new App_Main();
        }
        return instancie;
    }

    public UserRepository getUserRepository()
    {
        return userRepository;
    }

    public void setComponent(App_Component component)
    {
        this.component = component;
    }

    public void setUserRepository(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instancie=this;
        Timber.plant(new Timber.DebugTree());

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {


        }
        else if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED)
        {
            component = DaggerApp_Component.builder()
                    .app_ContextModule(new App_ContextModule(this))
                    .build();
            userRepository = component.ActivateUserRespository();
        }
    }


}
