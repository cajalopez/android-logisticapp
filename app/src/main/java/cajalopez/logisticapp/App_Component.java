package cajalopez.logisticapp;

import cajalopez.logisticapp.Repository.UserRepository;
import cajalopez.logisticapp.Repository.UserRepository_Module;
import dagger.Component;

@App_Scope
@Component(modules = UserRepository_Module.class)
public interface App_Component
{

    UserRepository ActivateUserRespository();

}
