package cajalopez.logisticapp.View.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cajalopez.logisticapp.R;

/**
 * Created by cajalopez on 7/14/17.
 */

public class Dialog_Fragment extends DialogFragment {

    private View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.dialog_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @OnClick(R.id.ok_button)
    public void ok()
    {

    }

    @OnClick(R.id.cancel_button)
    public void cancel()
    {

    }

}
