package cajalopez.logisticapp.View.Fragments;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.zetterstrom.com.forecast.models.Forecast;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import cajalopez.logisticapp.App_Main;
import cajalopez.logisticapp.R;
import cajalopez.logisticapp.ViewModel.UserViewModel;
import timber.log.Timber;

public class Main_Fragment extends Fragment implements LifecycleRegistryOwner {

    private LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private View view;
    private UserViewModel model;

    @BindView(R.id.current) TextView current;
    @BindView(R.id.locale) TextView locale;
    @BindView(R.id.wind) TextView wind;
    @BindView(R.id.humedity) TextView humedity;
    @BindView(R.id.latitud) TextView latitud;
    @BindView(R.id.longitud) TextView longitud;

    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        model = ViewModelProviders.of(this).get(UserViewModel.class);
        model.init();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.assembly_fragment, container, false);
        ButterKnife.bind(this, view);
        current.setVisibility(View.INVISIBLE);
        locale.setVisibility(View.INVISIBLE);
        wind.setVisibility(View.INVISIBLE);
        humedity.setVisibility(View.INVISIBLE);
        model.getWeathertLiveData().observe(this, new Observer<Forecast>()
        {
            @Override
            public void onChanged(@Nullable Forecast forecast)
            {

                current.setVisibility(View.VISIBLE);
                locale.setVisibility(View.VISIBLE);
                wind.setVisibility(View.VISIBLE);
                humedity.setVisibility(View.VISIBLE);

                current.setText("Temperature\n"+String.valueOf(forecast.getCurrently().getTemperature())+"•F");
                locale.setText(forecast.getTimezone());
                wind.setText("Wind Speed\n"+forecast.getCurrently().getWindSpeed().toString()+"Mph");
                humedity.setText("Humidity\n"+String.valueOf(forecast.getCurrently().getHumidity()+"%"));
            }
        });

        latitud.setVisibility(View.INVISIBLE);
        longitud.setVisibility(View.INVISIBLE);

        model.getLocation().observe(this, new Observer<Location>()
        {
            @Override
            public void onChanged(@Nullable Location location)
            {

                latitud.setVisibility(View.VISIBLE);
                longitud.setVisibility(View.VISIBLE);

                DecimalFormat df = new DecimalFormat();
                df.setMaximumFractionDigits(5);
                latitud.setText("Latitude "+String.valueOf(df.format(location.getLatitude())));
                    longitud.setText("Longitude "+String.valueOf(df.format(location.getLongitude())));
            }

        });

        return view;
    }

    @Override
    public LifecycleRegistry getLifecycle()
    {
        return lifecycleRegistry;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
