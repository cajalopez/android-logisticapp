package cajalopez.logisticapp.View.Activities;

import android.Manifest;
import android.app.Fragment;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import cajalopez.logisticapp.App_Component;
import cajalopez.logisticapp.App_ContextModule;
import cajalopez.logisticapp.App_Main;
import cajalopez.logisticapp.DaggerApp_Component;
import cajalopez.logisticapp.R;
import cajalopez.logisticapp.View.Fragments.Main_Fragment;
import cajalopez.logisticapp.ViewModel.UserViewModel;


public class Activity_Main extends AppCompatActivity implements LifecycleRegistryOwner {

    LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_view)
    NavigationView navigation_view;
    @BindView(R.id.drawer)
    DrawerLayout drawer;
    private UserViewModel model;
    private App_Component component;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.assembly_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setupDrawer(navigation_view);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.open,
                R.string.close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.INTERNET},3);
        }
        else if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED)
        {
            model = ViewModelProviders.of(this).get(UserViewModel.class);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            Main_Fragment main_fragment = new Main_Fragment();
            transaction.add(R.id.sub_frame, main_fragment,"main fragment");
            transaction.commit();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 4: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    component = DaggerApp_Component.builder()
                    .app_ContextModule(new App_ContextModule(App_Main.getInstance().getApplicationContext()))
                    .build();
                    component.ActivateUserRespository();

                    App_Main.getInstance().setComponent(component);
                    App_Main.getInstance().setUserRepository(component.ActivateUserRespository());

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    Main_Fragment main_fragment = new Main_Fragment();
                    transaction.add(R.id.sub_frame, main_fragment,"main fragment");
                    transaction.commit();

                }
                else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED)
                {
                    PermissionDialogAlert();
                }
                return;
            }
            case 3:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},4);
                }
                return;
            }
        }
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    private void setupDrawer(NavigationView navigation_view) {

        navigation_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                item.setChecked(false);
                drawer.closeDrawers();

                switch (item.getItemId()) {

                    case R.id.ramdon:
                        model.init();
                        break;

                }
                return true;
            }
        });
        View headerLayout = navigation_view.getHeaderView(0);
    }

    public void PermissionDialogAlert()
    {
        //DIALOG FRAGMENT - SHOW RATIONALE
        finish();
    }



}
